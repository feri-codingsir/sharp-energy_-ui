import { fileURLToPath, URL } from 'node:url'

import { defineConfig } from 'vite'
import vue from '@vitejs/plugin-vue'
import vueJsx from '@vitejs/plugin-vue-jsx'
import AutoImport from 'unplugin-auto-import/vite'
import Components from 'unplugin-vue-components/vite'
import { ElementPlusResolver } from 'unplugin-vue-components/resolvers'

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [
    vue(),
    vueJsx(),
    // ...
    AutoImport({
      resolvers: [ElementPlusResolver()]
    }),
    Components({
      resolvers: [ElementPlusResolver()]
    })
    // ...
  ],
  resolve: {
    alias: {
      '@': fileURLToPath(new URL('./src', import.meta.url)),
      '~mock': fileURLToPath(new URL('./mock', import.meta.url))
    }
  },
  // ...
  css: {
    preprocessorOptions: {
      // define global scss variable
      scss: {
        additionalData: `@import '@/styles/variables.scss';`
      }
    }
  },
  server: {
    proxy: {
      '^/fnapi/.*': {
        target: 'http://qf.ft.learv.com',
        changeOrigin: true,
        rewrite: (path) => path.replace(/^\/fnapi/, '')
      },
      '^/dcmapi/.*': {
        target: 'https://qf.dcm.learv.com:3005',
        changeOrigin: true,
        rewrite: (path) => path.replace(/^\/dcmapi/, '')
      }
      // ...
      // ..
    }
  }
  // ...
})
