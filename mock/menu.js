export default [
  {
    auth_id: 111,
    icon: 'icon-yibiaopan',
    auth_name: '仪表盘',
    path: '/dashboard',
    type: 1, // 1-菜单、2-按钮
    auth_pid: 0,
    auth_pname: '',
    component: '',
    sort: 111,
    children: []
  },
  {
    auth_id: 222,
    icon: 'icon-shezhi',
    auth_name: '系统管理',
    path: '',
    type: 1,
    auth_pid: 0,
    auth_pname: '',
    component: '',
    sort: 222,
    children: [
      {
        auth_id: 2221,
        icon: 'icon-icon-user',
        auth_name: '用户管理',
        path: '/system/user',
        type: 1,
        auth_pid: 222,
        auth_pname: '系统管理',
        component: '@/views/system/user/index.vue',
        sort: 2221
      },
      {
        auth_id: 2222,
        icon: 'icon-jiaoseguanli',
        auth_name: '角色管理',
        path: '/system/role',
        type: 1,
        auth_pid: 222,
        auth_pname: '系统管理',
        component: '@/views/system/role/index.vue',
        sort: 2222
      },
      {
        auth_id: 2223,
        icon: 'icon-caidan',
        auth_name: '菜单管理',
        path: '/system/menu',
        type: 1,
        auth_pid: 222,
        auth_pname: '系统管理',
        component: '@/views/system/menu/index.vue',
        sort: 2223
      },
      {
        auth_id: 2224,
        icon: 'icon-zidianguanli',
        auth_name: '字典管理',
        path: '/system/dict',
        type: 1,
        auth_pid: 222,
        auth_pname: '系统管理',
        component: '@/views/system/dict/index.vue',
        sort: 2224
      },
      {
        auth_id: 2225,
        icon: 'icon-rizhi',
        auth_name: '日志管理',
        type: 1,
        auth_pid: 222,
        auth_pname: '系统管理',
        sort: 2225,
        children: [
          {
            auth_id: 22251,
            icon: 'icon-caozuorizhi',
            auth_name: '操作日志',
            path: '/system/log/oper',
            type: 1,
            auth_pid: 2225,
            auth_pname: '系统管理',
            component: '@/views/system/log/oper.vue',
            sort: 22251
          },
          {
            auth_id: 22252,
            icon: 'icon-guanlidenglurizhi',
            auth_name: '登录日志',
            path: '/system/log/login',
            type: 1,
            auth_pid: 2225,
            auth_pname: '系统管理',
            component: '@/views/system/log/login.vue',
            sort: 22252
          }
        ]
      },
      {
        auth_id: 2226,
        icon: 'icon-xitongxiaoxi',
        auth_name: '系统消息',
        path: '/system/notice',
        type: 1,
        auth_pid: 222,
        auth_pname: '系统管理',
        component: '@/views/system/notice/index.vue',
        sort: 2225
      }
    ]
  },
  {
    auth_id: 333,
    icon: 'icon-quanqudaofuwujiankong',
    auth_name: '系统监控',
    path: '',
    type: 1,
    auth_pid: 0,
    auth_pname: '',
    component: '',
    sort: 6,
    children: [
      {
        auth_id: 3331,
        icon: 'icon-a-zaixianyonghu2',
        auth_name: '在线用户',
        path: '/monitor/online',
        type: 1,
        auth_pid: 333,
        auth_pname: '系统监控',
        component: '@/views/monitor/online/index.vue',
        sort: 3331
      },
      {
        auth_id: 3332,
        icon: 'icon-jiankongzhongxin',
        auth_name: '服务监控',
        path: '/monitor/server',
        type: 1,
        auth_pid: 333,
        auth_pname: '系统监控',
        component: '@/views/monitor/server/index.vue',
        sort: 3332
      },
      {
        auth_id: 3333,
        icon: 'icon-redis',
        auth_name: '缓存监控',
        path: '/monitor/cache',
        type: 1,
        auth_pid: 333,
        auth_pname: '系统监控',
        component: '@/views/monitor/cache/index.vue',
        sort: 3333
      }
    ]
  }
]
