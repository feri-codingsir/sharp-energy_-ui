import { createRouter, createWebHashHistory } from 'vue-router'

const router = createRouter({
  history: createWebHashHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/login',
      component: () => import('@/views/login.vue')
    },
    {
      path: '/',
      redirect: '/dashboard',
      component: () => import('@/layout/index.vue'),
      children: [
        // ...
        {
          path: 'dashboard',
          component: () => import('@/views/dashboard/index.vue'),
          meta: { pname: '仪表盘', name: '' }
        },
        {
          path: 'system/user',
          component: () => import('@/views/system/user/index.vue'),
          meta: { pname: '系统管理', name: '用户管理' }
        },
        {
          path: 'system/role',
          component: () => import('@/views/system/role/index.vue'),
          meta: { pname: '系统管理', name: '角色管理' }
        },
        {
          path: 'system/menu',
          component: () => import('@/views/system/menu/index.vue'),
          meta: { pname: '系统管理', name: '菜单管理' }
        },
        {
          path: 'system/dict',
          component: () => import('@/views/system/dict/index.vue'),
          meta: { pname: '系统管理', name: '字典管理' }
        },
        {
          path: 'system/log/oper',
          component: () => import('@/views/system/log/oper.vue'),
          meta: { pname: '系统管理', name: '操作日志' }
        },
        {
          path: 'system/log/login',
          component: () => import('@/views/system/log/login.vue'),
          meta: { pname: '系统管理', name: '登录日志' }
        },
        {
          path: 'system/notice',
          component: () => import('@/views/system/notice/index.vue'),
          meta: { pname: '系统管理', name: '系统消息' }
        },
        {
          path: 'monitor/online',
          component: () => import('@/views/monitor/online/index.vue'),
          meta: { pname: '系统监控', name: '在线用户' }
        },
        {
          path: 'monitor/server',
          component: () => import('@/views/monitor/server/index.vue'),
          meta: { pname: '系统监控', name: '服务监控' }
        },
        {
          path: 'monitor/cache',
          component: () => import('@/views/monitor/cache/index.vue'),
          meta: { pname: '系统监控', name: '缓存监控' }
        }
        // ...
      ]
    },
    {
      path: '/:pathMatch(.*)',
      component: () => import('@/views/404.vue')
    }
  ]
})

export default router
