// 导入axios
import axios from 'axios'

// 创建 request 实例
const request = axios.create({
  baseURL: '/fnapi',
  headers: {
    'content-type': 'application/x-www-form-urlencoded'
  }
})

// 拦截器
request.interceptors.request.use(
  (config) => {
    config.headers.token = JSON.parse(localStorage.getItem('info'))?.token
    return config
  },
  (error) => Promise.reject(error)
)
request.interceptors.response.use(
  (response) => {
    return response.data
  },
  (error) => Promise.reject(error)
)

// 导出
export default request
