export const checkMobile = (rules, value, callback) => {
  if (/^1\d{10}$/.test(value)) {
    return callback()
  } else {
    return callback(new Error('手机号格式有误'))
  }
}

export const checkPassword = (rules, value, callback) => {
  // 没输入数据不验证
  if (!value) return callback()
  // 输入了
  if (/^\w{6,12}$/.test(value)) {
    return callback()
  } else {
    return callback(new Error('密码只能输入6~12个字符'))
  }
}

export const checkCode = (rules, value, callback) => {
  if (/^\w{4}$/.test(value)) {
    return callback()
  } else {
    return callback(new Error('验证码只能输入4个字'))
  }
}
