/*
import {useConfirm} from '@/hooks/useConfirm'

useConfirm()
*/

import { ElMessage, ElMessageBox } from 'element-plus'

export const useConfirm = () => {
  ElMessageBox.confirm('确定删除该条数据吗?', 'QFADMIN温馨提示', {
    confirmButtonText: '确定',
    cancelButtonText: '取消',
    type: 'warning'
  })
    .then(() => {
      ElMessage({
        type: 'success',
        message: '操作成功'
      })
    })
    .catch(() => {
      ElMessage({
        type: 'info',
        message: '用户取消'
      })
    })
}
