// 导入模块
import router from '@/router'
import NProgress from 'nprogress'
import 'nprogress/nprogress.css'
import { useLoading } from '@/hooks/useLoading'

const { show, hide } = useLoading()

// 首次访问或者刷新网页就会执行
let isRefresh = true

// 导航守卫   每次切换路由都会触发
router.beforeEach(async (to, from, next) => {
  NProgress.start()

  if (isRefresh) {
    show()
    isRefresh = false // 切记除非网页刷新否则一直是false不再执行
  }

  // next()
  // 1、指定页面直接访问（术语白名单 例如404 login），其他页面判断
  // 2 获取仓库数据或者h5数据  没有-跳转到登录页、有-正常访问
  if (['/404', '/login'].includes(to.path)) {
    next()
  } else {
    const info = localStorage.getItem('info')
    if (info) {
      next()
    } else {
      next({ path: '/login' })
    }
  }
})

router.afterEach(() => {
  if (!isRefresh) hide()
  NProgress.done()
})
